<!DOCTYPE html>
<html lang="fr">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<title>calendrier</title>
	<link rel="stylesheet" href="style.css" type="text/css"/>
	<!--<script type="text/javascript" src="jquery-2.1.3.min.js" defer></script>-->
	<!--<script type="text/javascript" src="javascript.js" defer></script>-->
</head>

<body>

<?php

include 'cal.php';
$calendar = new Calendar();
// on demande d'afficher 4 options
$calendar->display_option(4);

include 'highlight_today.php';
$highlightToday = new HighlightToday();
$calendar->attachObserver('set_today',$highlightToday);

include 'get_schedule.php';
$get_schedule = new get_schedule();
$calendar->attachObserver('check_schedule',$get_schedule);

echo $calendar->show();



?>

</body>
</html>