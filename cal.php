
<?php

class Calendar {

	/**
     * Constructor
     */
	public function __construct(){


		$this->naviHref = htmlentities($_SERVER['PHP_SELF']);

		// GET URL pour l'année
		if (isset($_GET['year'])) {
			$this->year = $_GET['year'];
		} else {
			$this->year = (new DateTime)->format("Y");
		}

		// GET URL pour la semaine
		if (isset($_GET['week'])) {
			$this->week = $_GET['week'];
		} else {
			$this->week = (new DateTime)->format("W");
		}


		// GET URL LAYOUT pour la semaine
		if (isset($_GET['layout'])) {
			$this->layout = $_GET['layout'];
			if ($this->layout == "inverted") {
				$this->layout_toggle = "normal";
				$this->layout = "inverted";
			} else {
				$this->layout_toggle = "inverted";
				$this->layout = "normal";
			}
		} else {
			$this->layout = "normal";
			$this->layout_toggle = "inverted";
		}
	}
	
	/********************* PROPERTY ********************/

	// observer
	public $today_class = "";
	public $current_date_schedule = "";
	public $current_class_level = "";
	protected $observers = array();
	
    private $current_date_start = null;
    private $current_date_end = null;
    private $currentDate = null;
    private $current_week_start_year = 0;
    private $current_week_number = 0;
    private $current_weeks_count = 0;

    private $next_week_date_start = null;
    private $next_year = 0;
    private $next_week_number = 0;
    private $previous_week_date_end = null;
    private $previous_year = 0;
    private $previous_week_number = 0;

    private $number_of_option = 0;

	private $naviHref = null;
	
    
    /********************* PUBLIC **********************/  
	/**
	* @return void
	* @access public
	*/
	public function attachObserver($type,$observer) {
		$this->observers[$type][]=$observer;
	}
	
	/**
	* @return void
	* @access public
	*/
	public function notifyObserver($type) {
		if(isset($this->observers[$type])){
			foreach($this->observers[$type] as $observer ){
				$observer->update($this);
			}
		}
	}

	public function getCurrentDate(){
		return $this->currentDate;
	}

	public function get_current_class_level(){
		return $this->current_class_level;
	}

	public function display_option($number) {
		return $this->number_of_option = $number;
	}

	/**
    * crée un calendrier par semaines
    *
    * @access              public
    * @param               string
    * @param               string
    * @param               array
    * @return              string
    */
	public function show($attributes=false){
		// Les dates en FR
		setlocale(LC_TIME, "fr_FR");

		// Création des dates de début et fin de semaine
		$this->current_date_start = (new DateTime)->setISODate($this->year,$this->week);
		$this->current_date_end = (clone $this->current_date_start)->modify("+6 days");
		$this->_create_variables();
		// echo "<br>"; print_r($this->current_date_start);
		// echo "<br>"; print_r($this->current_date_end);

		$period = $this->_calculate_interval($this->current_date_start,"P1D",$this->current_date_end);

		$content = '<header>'.$this->_create_navigation().'</header>';

		$content .= '<section>';

		$content .= '<aside>'.$this->_create_weeks_browser().'</aside>';

		$content .= '<main class="'.$this->layout.'">';

		$content .= '<div class="empty">Jours</div>';
		$content .= $this->_create_day_label($period);
		
		$content .= '<div class="empty">Dates</div>';
		foreach ($period as $day) {
			$content .= $this->_create_day_number($day);
		}

		for ($i = 2; $i < ($this->number_of_option + 2); $i++) {
			$this->current_class_level = "DG".$i;

			$content .= '<div class="empty">DG'.$i.'</div>';
			foreach ($period as $day) {
				$content .= $this->_create_day_for_an_option($day);
			}
		
		}		

		$content .= '</main>';
		$content .= '</section>';

		return $content;
	}
	

	/********************* PRIVATE **********************/  
    /**
    * crée les jours d'une option
    *
    * @access              private
    * @param               date
    * @return              string
    */
	private function _create_day_for_an_option($day) {
		$this->current_date_schedule = "";
		$this->currentDate = $day->format("Y-m-d"); // on met à jour la date courante
		$this->notifyObserver('check_schedule'); // on observe si on a quelque chose à faire ce jour

		$content = '';
		$content .= "<div class='content'>".$this->current_date_schedule."</div>";
		
		return $content;
	}



	/********************* PRIVATE **********************/  
    /**
    * crée des variables dates
    *
    * @access              private
    */
	private function _create_variables() {
		$this->today = new DateTime("today");
		$this->today_year = $this->today->format("Y");
		$this->today_week = $this->today->format("W");

		$this->current_week_start_year = ($this->current_date_start)->format("Y");
		$this->current_week_end_year = ($this->current_date_end)->format("Y");
		$this->current_week_number = ($this->current_date_start)->format("W");
		$this->current_weeks_count = $this->_get_weeks_count(($this->current_week_start_year));
	
		$this->previous_week_date_end = clone ($this->current_date_start);
		$this->previous_week_date_end->modify("-1 day");
		$this->next_week_date_start = clone ($this->current_date_end);
		$this->next_week_date_start->modify("+1 day");

		$this->next_year =	($this->next_week_date_start->format("W")) == 1 ? 
							$this->current_week_start_year + 1 : $this->current_week_end_year;
		$this->next_week_number =	$this->current_week_number == $this->current_weeks_count ?
									1 : ($this->current_week_number) + 1;
		$this->previous_year = 	$this->current_week_number == 1 ?
								$this->current_week_end_year - 1 : $this->previous_week_date_end->format("Y");
		$this->previous_week_number = 	$this->current_week_number == 1 ?
										$this->previous_week_date_end->format("W") : ($this->current_week_number) - 1;
	}


	/********************* PRIVATE **********************/  
    /**
    * crée l'affichage des numéros de semaines pour deux trimestres
    *
    * @access              private
    * @return              string
    */
	private function _create_weeks_browser() {
		// On met en place l'affichage des semaines d'une année scolaire
		// à partir de la semaine 35 (en septembre), on dit que l'on passe à un nouveau duo d'années
		// alors entre 1 et 35, [année - 1; année]
		// et entre 36 et 52, [année; année + 1]

		if ($this->current_week_number < 36) {
			$first_year = $this->current_week_start_year - 1;
			$second_year = $this->current_week_start_year;
		} elseif ($this->current_week_number >= 27) {
			$first_year = $this->current_week_start_year;
			$second_year = $this->current_week_start_year + 1;
		}

		$start_semester_1 = new DateTime("first day of September ".$first_year);
		$end_semester_1 = new DateTime("last day of February ".$second_year);
		$start_semester_2 = new DateTime("first day of March ".$second_year);
		$end_semester_2 = new DateTime("last day of June ".$second_year);

		$period_semester_1 = $this->_calculate_interval($start_semester_1, "P1M", $end_semester_1);
		$period_semester_2 = $this->_calculate_interval($start_semester_2, "P1M", $end_semester_2);
		$school_year = [$period_semester_1,$period_semester_2];
		$school_semesters_years = ["Semestre 1","Semestre 2"];

		$content = "<p>".$first_year."</p>";
		foreach ($school_year as $key=>$semester) {
			$content .= "<p class='semester'>".$school_semesters_years[$key]."</p>";
			foreach ($semester as $month) {
				$content .= "<span>".utf8_encode(strftime("%B",$month->getTimestamp()))."</span> ";
				
				$end_month = clone $month;
				$end_month->modify('last day of this month');
				$interval_one_week = new DateInterval("P1W");
				$period_months = new DatePeriod($month, $interval_one_week, $end_month);
				foreach ($period_months as $week) {
					$content .= "<a href='?year=".$month->format("Y")."&week=".$week->format('W')."&layout=".$this->layout."'>".$week->format('W')."</a> ";
				}
				// mise en place de l'arrivée d'une nouvelle année
				$next_month = (clone $month)->modify('+1 month');
				if ($next_month->format("Y") != $month->format("Y")) {
					$content .= "<p>".$next_month->format("Y")."</p>";
				} else {
				$content .= "<br>";
				}
			}
		}
		return $content;
	}

	/********************* PRIVATE **********************/  
    /**
    * calcule un DatePeriod
    *
    * @access              private
    * @param               date
    * @param               string
    * @param               date
    * @return              date period
    */
	private function _calculate_interval($start, $interval, $end) {
		$end->modify("+1 day"); // ajout d'un jour que DatePeriod fonctionne bien
		$period_output = new DatePeriod($start, new DateInterval($interval), $end);
		return $period_output;
	}


	/********************* PRIVATE **********************/  
    /**
    * crée les numéros des jours avec leur mois
    *
    * @access              private
    * @param               date
    * @return              string
    */
	private function _create_day_number($day) {
		$this->today_class = ""; // pour un jour autre qu'aujourd'hui
		$this->currentDate = $day->format("Y-m-d"); // on met à jour la date courante
		$this->notifyObserver('set_today'); // on observe si on est aujourd'hui, dans highlight_today.php

		$day_number = $day->format("d").' '.utf8_encode(strftime("%B",$day->getTimestamp()));
		$content = '';
		$content .= "<div class='dates".$this->today_class."'>";
		$content .= "<abbr title=".$day->format("d").".".$day->format("m").">".$day_number;
		$content .= "</abbr></div>";
		
		return $content;
	}

	/**
    * crée le nom des jours de la semaine
    *
    * @access              private
    * @param               string
    * @return              string
    */
	private function _create_day_label($period){
		$content = '';
		foreach ($period as $day) {
			$day_name_fr = utf8_encode(strftime("%A",$day->getTimestamp()));
			$day_initial = substr($day_name_fr, 0, 1);
			$content .= "<div class='jours'><abbr title=".$day_initial.">".$day_name_fr."</abbr></div>";
		}
		return $content;
	}



	/**
    * crée la navigation : toggle / précédent / suivant / aujourd'hui
    *
    * @access              private
    * @return              string
    */
	private function _create_navigation(){
		
		$content = '<a class="toggle" href="?year='.$this->current_week_start_year.'&week='.$this->current_week_number.'&layout='.$this->layout_toggle.'">Toggle</a>';
		$content .= '<a class="previous" href="?year='.$this->previous_year.'&week='.$this->previous_week_number.'&layout='.$this->layout.'">Précédent</a>';
		$content .= '<a class="next" href="?year='.$this->next_year.'&week='.$this->next_week_number.'&layout='.$this->layout.'">Suivant</a>';
		$content .= '<a class="today" href="?year='.$this->today_year.'&week='.$this->today_week.'&layout='.$this->layout.'">Aujourd\'hui</a>';
		return $content;

	}


	/**
    * calcule le nombre de semaines dans une année
    *
    * @access              private
    * @param               date
    * @return              number
    */
	private function _get_weeks_count($year){
		$date = new DateTime;
	    $date->setISODate($year, 53);
	    return ($date->format("W") === "53" ? 53 : 52);
	}


} // fin class Calendar


