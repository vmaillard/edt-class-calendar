<?php 
/**
*@author  				The-Di-Lab
*@email   				thedilab@gmail.com
*@website 				www.the-di-lab.com
*@version              1.0
**/
class HighlightToday {
        public function update(Calendar $cal){
        	
        	$today = (new DateTime("today"))->format("Y-m-d");
        	if($today==$cal->getCurrentDate()){
        		$cal->today_class= ' today';
        	}
        	
        }
}
?>