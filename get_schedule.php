<?php 

/*

array(
	'id' => '2130',
	'date_reservation' => '2018-02-01',
	'a_noter_couleur' => '0',
	'a_noter' => '',
	'cursus_couleur' => '1',
	'cursus_mensuel' => '',
	'texte_cursus' => 'Du travail',
	'cursus' => '2',
	'periode' => 'AM'
	),
*/

class get_schedule {

	    private $schedule = array (
		array(
		        'date' => '2018-02-01',
		        'content' => 'Du travail',
		        'classlevel' => 'DG2'
		    ),
		array(
		        'date' => '2018-02-02',
		        'content' => 'Du travail pour la DG3',
		        'classlevel' => 'DG3'
		    ),
		array(
		        'date' => '2018-02-02',
		        'content' => 'Toujours plus de travail',
		        'classlevel' => 'DG2'
		    ),
		array(
		        'date' => '2018-02-02',
		        'content' => 'Toujours beaucoup plus de travail',
		        'classlevel' => 'DG5'
		    ),
		array(
		        'date' => '2018-02-09',
		        'content' => 'Toujours plus de travail',
		        'classlevel' => 'DG3'
		    )
		);

        public function update(Calendar $cal){

        	// à chaque nouveau jour, on cherche s'il y des données
        	// s'il y en a, pour le jour en cours, on vérifie si la date correspond à l'option
        	// si c'est le cas, on ajoute le contenu

        	$keys = array_keys(array_column($this->schedule, 'date'), $cal->getCurrentDate());

			foreach ($keys as $key) {
				$stored_date = array_column($this->schedule, 'date')[$key];
				$stored_content = array_column($this->schedule, 'content')[$key];
				$stored_class_level = array_column($this->schedule, 'classlevel')[$key];

				if ($cal->getCurrentDate() == $stored_date && $stored_class_level == $cal->get_current_class_level()) {
					$cal->current_date_schedule = $stored_content;
				}

			}
        	
        }
} // fin class get_schedule
?>


